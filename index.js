/*
	
*/

const express = require("express");
const port = 4000;
const app = express();

// mongoose - package that allows creation of schemas to model our data structure
const mongoose = require("mongoose");
mongoose.connect("mongodb+srv://admin:admin123@course-booking.2xxzg.mongodb.net/B157_to_do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Database connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

const Task = mongoose.model("Task", taskSchema);


app.use(express.json());

// allows our app to read data from forms
app.use(express.urlencoded({extended:true}));


/* Create a new Task

	1. Add a functionality to check if there are duplicate tasks.
		- If the task already exists in the database, we return an error.
		- If the task doesn't exist, we add it in our database.

	 2. The task data will be coming from the request's body.

	 3. Create a Task object with a "name" field/property.
*/

app.post('/tasks', (req, res) => {

	Task.findOne({name: req.body.name}, (err, result) => {

		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {

				if(saveErr) {
					return console.log(saveErr)
				} else {
					return res.status(201).send("New task created.")
				}
			})
		}
	})
})


/* GET request to retrieve all the documents

	1. Retrieve all the documents.
	2. If an error is encountered, print the error.
	3. If no errors are found, send a success back to the client.
*/

app.get('/tasks', (req, res) => {

	Task.find({}, (err, result) => {

		if(err) {
			console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.get('/hello', (req, res) => {
	res.send("hello World.")
});

/* ------------------ ACTIVITY ---------------------*/
/*
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.
*/

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	}
})

const User = mongoose.model("User", userSchema);

app.post('/signup', (req, res) => {

	if(req.body.username == null || req.body.username == '' || req.body.password == null || req.body.password == '') {

		return res.send("username or password can't be blank.")

	} else {

		User.findOne({username: req.body.username}, (error, result) => {

			if(result != null && result.username == req.body.username) {
				return res.send("Duplicate user found. Enter a unique username.")
			} else {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})

				newUser.save((saveError, saveSuccess) => {

					if(saveError) {
						return console.log(saveError)
					} else {
						return res.status(201).send(`New user registered: ${req.body.username}`)
					}
				})
			}
		})
	}	
})

app.listen(port, () => {
	console.log(`Server running at port: ${port}.`)
})
